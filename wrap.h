#pragma once
#include <stdarg.h>
#include <stdio.h>

enum { N_WRAP_OPTS = 7 };
struct wrap_opts {
	union {
		struct {
			int tab_size;
			int cols;
			int second_line_indent_incr;
			int first_line_indent;
			int initial_tabs;

			// This is a number of spaces, not tabs.
			int overall_indent;

			// In case you need to print some text and then your wrapped text,
			// like this: "hello: <your wrapped text>". Pass
			// .to_subtract_from_first_line = 7 in that case.
			int to_subtract_from_first_line;
		};
		int arr[7];
	};
};
typedef struct wrap_opts wrap_opts;

// Use this for default values. Set it at the start of your program.
// As for what happens if your libraries want to have default opts,
// well, they can't, can they? You could do something more complex
// involving IDs or something, I dunno.
extern struct wrap_opts your_default_wrap_opts;

int wrap_ap (char *pr, size_t n_pr, struct wrap_opts *this_run_opts, char *fmt,
		va_list ap);
int wrap (char *out_buf, size_t n_out_buf, struct wrap_opts *this_run_opts,
		char *fmt, ...);
int wrap_print (FILE *f, char *out_buf, size_t n_out_buf, struct wrap_opts *this_run_opts,
		char *fmt, ...);
