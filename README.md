# Wrap

## Technical description

This is word wrap. It seems to work alright.

The only thing remotely interesting about it is you can get it to
increment the post-wrap line by more, so it's like this (assume we're
wrapping at ten):

```
1234 6 89
    wrap
    still
    wrapped
```

Obviously it looks less stupid when wrapping at greater numbers.

## Technical installations instructions

Either you can `git submodule add
https://gitlab.com/objection/wrap.git dest` or you can:

```
git clone 'https://gitlab.com/objection/wrap.git'
cd wrap
meson build
ninja -Cbuild install
```

You'll need Meson to do that, which you can get from your package
manager.

You can just copy over the two files, too.
