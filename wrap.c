#if 0
gcc -g3 -Wall -o format_output_2 format_output_2.c
exit
#endif
#include "wrap.h"
#include <ctype.h>
#include <assert.h>
#include <string.h>
#include <threads.h>
#include <stdbool.h>

#define $fori(_idx, _max) 																\
	for (__typeof__ (_max + 0) _idx = 0; _idx < _max; _idx++)

struct globals {
	struct wrap_opts opts;
	int n_lines, cols, this_n_tabs;
	char *q, *start_of_line, *s, *e;

};
thread_local struct globals g;

enum wrap_token_kind {
	TOKEN_KIND_WORD,
	TOKEN_KIND_SPACES,
	TOKEN_KIND_TAB,
	TOKEN_KIND_NEWLINE, // start of buf and '\n'
	TOKEN_KIND_END,
};

struct wrap_token {
	char *s, *e;
	enum wrap_token_kind kind;
};

struct wrap_opts your_default_wrap_opts;

thread_local static struct wrap_token w_t, last;

static void wrap_get_token () {
	last = w_t;
	w_t.s = w_t.e;
	if (w_t.s == g.e) {
		w_t.kind = TOKEN_KIND_END;
		return;
	};

	w_t.e = w_t.s + 1;
	switch (*w_t.s) {
		case '\t':
			w_t.kind = TOKEN_KIND_TAB;
			break;
		case '\n':
			w_t.kind = TOKEN_KIND_NEWLINE;
			break;
		case ' ':
			while (*w_t.e && *w_t.e == ' ')
				w_t.e++;
			w_t.kind = TOKEN_KIND_SPACES;
			break;
		default:
			if (w_t.s == g.e)
				w_t.kind = TOKEN_KIND_END;
			else {
				while (*w_t.e && !isspace (*w_t.e)) w_t.e++;
				w_t.kind = TOKEN_KIND_WORD;
			}
			break;
	}
}

static int wrap_get_n_tabs (void) {
	struct wrap_token save_token = last;
	int res = w_t.kind == TOKEN_KIND_TAB? 1: 0;
	last = w_t;
	while (wrap_get_token (), w_t.kind == TOKEN_KIND_TAB)
		res++;
	w_t = save_token;
	return res;
}

static int indent (int this_n_tabs, int initial_n_tabs) {
	$fori (i, this_n_tabs + initial_n_tabs) {
		$fori (j, g.opts.tab_size)
			*g.q++ = ' ';
	}
	return 0;
}

struct wrap_opts set_opts (struct wrap_opts *this_run_opts) {

	struct wrap_opts r = your_default_wrap_opts;
	if (!this_run_opts)
		return r;
	int n_default_opts_set = 0;
	for (int i = 0; i < N_WRAP_OPTS; i++) {
		if (r.arr[i])
			n_default_opts_set++;
		if (this_run_opts->arr[i])
			r.arr[i] = this_run_opts->arr[i];
	}
	assert (n_default_opts_set);
	return r;
}

static int back_up_over_spaces () {
	if (isspace (g.q[-1])) {
		g.q--;
		while (isspace (*g.q))
			g.q--;
		g.q++;
	}

	return 0;
}

static void add_newline () {
	*g.q++ = '\n';
	g.n_lines++;
}

static void add_overall_indent () {
	for (int i = 0; i < g.opts.overall_indent; i++)
		*g.q++ = ' ';
}

static int parse_newline () {
	g.n_lines++;

	// We've hit a newline; make sure g.cols isn't the value got
	// from to_subtract_from_first_line but the proper cols.
	back_up_over_spaces ();
	add_newline ();
	g.cols = g.opts.cols;
	while (wrap_get_token (), w_t.kind == TOKEN_KIND_NEWLINE)
		add_newline ();
	if (w_t.kind == TOKEN_KIND_END)
		return 0;
	g.this_n_tabs = wrap_get_n_tabs ();
	g.start_of_line = g.q;
	add_overall_indent ();
	indent (g.this_n_tabs, g.opts.initial_tabs);
	return 0;
}

static int parse_spaces () {
	if (g.q - g.start_of_line + (w_t.e - w_t.s) >= g.cols - 1)
		return 0;
	if (last.kind == TOKEN_KIND_NEWLINE)
		return 0;
	g.q = stpncpy (g.q, w_t.s, w_t.e - w_t.s);
	return 0;
}

static int parse_tab () {
	if (g.q - g.start_of_line + (w_t.e - w_t.s) >= g.cols - 1)
		return 0;
	for (int i = 0; i < g.opts.tab_size; i++)
		*g.q++ = ' ';
	return 0;
}

static int parse_word () {
	if (g.q - g.start_of_line + (w_t.e - w_t.s) >= g.cols - 1) {

		back_up_over_spaces ();

		add_newline ();

		// We've hit a newline; make sure g.cols isn't the value got
		// from to_subtract_from_first_line but the proper cols.
		g.cols = g.opts.cols;
		g.start_of_line = g.q;
		add_overall_indent ();
		indent (g.this_n_tabs + g.opts.second_line_indent_incr, g.opts.initial_tabs);

	}
	g.q = stpncpy (g.q, w_t.s, w_t.e - w_t.s);
	return 0;
}

int wrap_ap (char *pr, size_t n_pr, struct wrap_opts *this_run_opts, char *fmt,
		va_list ap) {

	g = (struct globals) {
		.opts = set_opts (this_run_opts),
		.q = pr,
		.start_of_line = pr
	};
	g.cols = g.opts.cols - g.opts.to_subtract_from_first_line;

	char fmted[n_pr];
	int rc = vsprintf (fmted, fmt, ap);
	if (rc < 0 || rc >= n_pr) return -1;

	w_t.e = fmted;
	g.s = fmted, g.e = strchr (fmted, '\0');

	// This isn't obvious: no overall indent if you've given the
	// to_subtract_from. The reason is I assume you're using it to
	// print some text then print the buffer returned by this library.
	// That's what you want to subtract. And in that case you don't
	// want an overall indent. But this won't do well if you *are*
	// using an overall indent and also want to use to_subtract_from.
	if (!g.opts.to_subtract_from_first_line) {
		add_overall_indent ();
	}
	indent (g.opts.first_line_indent, 0);
	if (g.q - pr >= n_pr)
		return -1;

	wrap_get_token ();
	if (w_t.kind == TOKEN_KIND_TAB)
		g.this_n_tabs = wrap_get_n_tabs ();

	indent (g.this_n_tabs, g.opts.initial_tabs);

	g.q = stpncpy (g.q, fmted, w_t.e - fmted);

	while (wrap_get_token (), w_t.kind != TOKEN_KIND_END) {
		switch (w_t.kind) {
			case TOKEN_KIND_NEWLINE: parse_newline (); break;
			case TOKEN_KIND_SPACES:  parse_spaces (); break;
			case TOKEN_KIND_TAB:     parse_tab (); break;
			case TOKEN_KIND_WORD:    parse_word (); break;
			default: assert (0);
		}
	}
	*g.q++ = '\0';

	if (g.q - pr >= n_pr)
		return -1;
	return g.q - pr;
}

int wrap (char *out_buf, size_t n_out_buf, struct wrap_opts *this_run_opts, char *fmt,
		...) {

	va_list ap;
	va_start (ap, fmt);
	int r = wrap_ap (out_buf, n_out_buf, this_run_opts, fmt, ap);
	va_end (ap);
	return r;
}

int wrap_print (FILE *f, char *out_buf, size_t n_out_buf, struct wrap_opts *this_run_opts,
		char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);
	int r = wrap_ap (out_buf, n_out_buf, this_run_opts, fmt, ap);
	va_end (ap);
	fprintf (f, "%s", out_buf);
	return r;
}

